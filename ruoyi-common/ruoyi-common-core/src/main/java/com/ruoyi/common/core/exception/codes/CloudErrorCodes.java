package com.ruoyi.common.core.exception.codes;


import com.common.zrd.validation.errorcode.BaseErrorCodes;
import com.common.zrd.validation.errorcode.ErrorCode;

/**
 * 功能描述  生产任务完成单错误码
 *
 * @author zrd
 * @date 2019/7/19
 * @return
 */
public class CloudErrorCodes extends BaseErrorCodes {


    /**
     * 对账错误码前缀
     */
    private static final String ERROR_CODE_COLLATE = "-40";
    public static final ErrorCode DIST_ERROR = custom(ERROR_CODE_COLLATE + "001", "数据字典接口异常！");

    static {
        initializeMessages(CloudErrorCodes.class);
    }
}
