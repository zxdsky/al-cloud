package com.ruoyi.job.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.job.entity.TestCase;
import com.ruoyi.job.mapper.TestCaseMapper;
import com.ruoyi.job.service.ITestCaseService;
import org.springframework.stereotype.Service;

/**
 * 测试用例Service业务层处理
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Service
public class TestCaseServiceImpl extends ServiceImpl<TestCaseMapper, TestCase> implements ITestCaseService {


}
