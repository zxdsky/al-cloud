package com.ruoyi.gen.factory;


public class TestServiceImplA implements TestService {

    @Override
    public String firstStep() {
        return "TestServiceImplA--firstStep";
    }

    @Override
    public String secondStep() {
        return "TestServiceImplA--secondStep";
    }

    @Override
    public String thirdStep() {
        return "TestServiceImplA--thirdStep";
    }
}
